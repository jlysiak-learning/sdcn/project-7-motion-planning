#!/bin/bash

cd `dirname $0`

set -x
N=$(( `cat /proc/stat | grep cpu | wc -l` - 1 ))
# Compile code.
mkdir -p build
cd build
cmake ..
make -j $N $*
