#!/bin/bash

for f in `ls src/*.h src/*.cpp`;
do
    clang-format -i $f
done
