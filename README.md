# Self-Driving Car Nanodegree: Path Planning

Project description: [link](./orig-README.md)

You can download the Term3 Simulator
[here](https://github.com/udacity/self-driving-car-sim/releases/tag/T3_v1.2).

Starter code can be found
[here](https://github.com/udacity/CarND-Path-Planning-Project).

## Directory contents

- `src`: project source code
- `install-linux.sh`: installs required tools, like cmake, gcc, libssl, etc.
  Also, downloads `uWebSockets` from github, builds it and copies important
  files: `libuWS.so` and include files.
- `build.sh`: just runs cmake and make
- `clean.sh`: ...
- `run.sh`: runs the app

## Building and running the code

1. Run `./install-linux.sh` to install deps.
2. Run `./build.sh`.
3. Run Term3 sim and start the game.
4. Run `./run.sh`.

### Solution

As suggested in Project Q&A session, the solution uses a single-header spline
[library](https://kluge.in-chemnitz.de/opensource/spline/spline.h) to generate
smooth (enough) trajectories.

The solution is encapsulated in `Planner` class in `src/planner.cpp`.
The main method (`generate_path`) is called from the event handler.
The planner's constructor takes map description to generate XY coords of
waypoints that are then used as spline's nodes. The spline lib generates `C^2`
curves that are sufficient for this task (acceleration and jerk do not exceeds
the limits).

Path generation starts from setting up car's reference data (`ref_car`) and two
initial spline's nodes in `spline_get_start_points`. If we don's have any
previously generated points, we need to bootstrap a spline from the current car
position and orientation. Otherwise, we can use previously generated data
(exactly last two points) and use them as initial two spline's nodes to generate
a continuous (and smooth) curve.

Then, we check sensor fusion data aka information about cars close to our car.
We are checking if the current lane is blocked by a slower car and we should
change the lane and whether we can do that.

Next, we generate new waypoints to use them as spline's nodes and transform the
nodes to car's frame of reference to easily interpolate a new path. It's also
important because the spline lib has this limitation that X points must be
monotonic. The easiest way is just to use car's coordinate system and
interpolate `y` points having well known `x` coord along the car direction.

After interpolation with spline, we transform the points back to the map
coordinates with a simple geometric transformation.
