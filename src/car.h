#ifndef CAR_H_
#define CAR_H_

#include <vector>


typedef enum {
    LANE_LEFT,
    LANE_CENTER,
    LANE_RIGHT,
    LANE_UNKNOWN
} Lane;

class Car {
public:
    Car();
    Car(std::vector<double> const & car_data);
    Car(double x, double  y, double s, double d, double yaw, double speed);

    bool same_lane(Car const & other_car) const;
    bool right_lane(Car const & other_car) const;
    bool left_lane(Car const & other_car) const;

    int id;
    double x;
    double y;
    double s;
    double d;
    double vx;
    double vy;
    double speed;
    double yaw;

    Lane  lane;

private:
    void set_lane();
};

#endif
