#include "planner.h"
#include "helpers.h"

#include <iostream>
#include <iomanip>

Planner::Planner(std::vector<double> const & map_waypoints_x,
                 std::vector<double> const & map_waypoints_y,
                 std::vector<double> const & map_waypoints_s) :
    map_waypoints_x(map_waypoints_x),
    map_waypoints_y(map_waypoints_y),
    map_waypoints_s(map_waypoints_s),
    logging(true)
{
    ref_car.lane = LANE_CENTER; // Start in the center of the road
}

void Planner::generate_path(Car const &car,
                       std::vector<Car> const & other_cars,
                       std::vector<double> const & previous_path_x,
                       std::vector<double> const & previous_path_y,
                       double end_path_s,
                       double end_path_d,
                       std::vector<double> &generated_path_xs,
                       std::vector<double> &generated_path_ys)
{
    log_car(car);
    log_path("PREVIOUS", previous_path_x, previous_path_y);

    const size_t pts_left = previous_path_x.size();

    std::vector<double> spline_xs;
    std::vector<double> spline_ys;

    // Get two initial spline points and setup reference car
    // position depending on how much points has been processed.
    spline_get_start_points(car, end_path_s, end_path_d, spline_xs, spline_ys,
                            previous_path_x, previous_path_y);
    check_other_cars(other_cars, pts_left);
    spline_add_waypoints(spline_xs, spline_ys);
    spline_transform_to_car_frame(spline_xs, spline_ys);

    // Define spline to interpolate predicted path points.
    tk::spline spline;
    spline.set_points(spline_xs, spline_ys);

    // Get all old points
    generated_path_xs.insert(generated_path_xs.begin(),
                             previous_path_x.begin(),
                             previous_path_x.end());
    generated_path_ys.insert(generated_path_ys.begin(),
                             previous_path_y.begin(),
                             previous_path_y.end());

    interpolate_dense_path_from_local_spline(spline,
                                             generated_path_xs,
                                             generated_path_ys);
}

void Planner::check_other_cars(std::vector<Car> const & other_cars, size_t pts_left)
{
    // Reset conditions
    lane_blocked_left = false;
    lane_blocked_right = false;
    lane_blocked_current = false;
    change_lane = false;

    for (Car const & other_car: other_cars) {
        // Compute other car's S position
        // If there are any points left, estimate car's future position
        // to match our car time point of `my_s`.
        double other_s = other_car.s + POINTS_TIME_SPACING * other_car.speed * pts_left;
        if (!ref_car.same_lane(other_car)) {
            continue;
        }
        if (other_s > ref_car.s && other_s - ref_car.s < CAR_CLOSE_THRESHOLD) {
            lane_blocked_current = true;
            ref_car.speed -= MAX_ACCEL * POINTS_TIME_SPACING * 2;
            if (ref_car.speed > other_car.speed) {
                change_lane = true;
            }
        }
    }
    if (logging) {
        std::cerr << "lane_blocked: " <<  lane_blocked_current << " ";
        std::cerr << "change_lane: " << change_lane << std::endl;
    }

    if (change_lane) {
        for (Car const & other_car: other_cars) {
            double other_s = other_car.s + POINTS_TIME_SPACING * other_car.speed * pts_left;
            if (other_s < ref_car.s - CHANGE_LANE_SPACE_THRESHOLD_BACK ||
                other_s > ref_car.s + CHANGE_LANE_SPACE_THRESHOLD) {
                // Enough space
                continue;
            }
            lane_blocked_left |= ref_car.left_lane(other_car);
            lane_blocked_right |= ref_car.right_lane(other_car);
        }
        if (!lane_blocked_left && ref_car.lane > LANE_LEFT) {
            ref_car.lane = ref_car.lane == LANE_CENTER ? LANE_LEFT : LANE_CENTER;
        } else if (!lane_blocked_right && ref_car.lane < LANE_RIGHT) {
            ref_car.lane = ref_car.lane == LANE_CENTER ? LANE_RIGHT : LANE_CENTER;
        }
    }
    // Lane free! Go faster!
    if (!lane_blocked_current && ref_car.speed < MAX_SPEED) {
        ref_car.speed += 2 * MAX_ACCEL * POINTS_TIME_SPACING;
    }

}


void Planner::interpolate_dense_path_from_local_spline(
                        tk::spline const & local_spline,
                        std::vector<double> & generated_path_xs,
                        std::vector<double> & generated_path_ys)
{
    // NOTE: Here we operate in car's local frame of reference!
    // Get points up to 30 meters ahead
    double local_x = WAYPOINTS_S_SPACING;
    // Get corresponding Y coordinate
    double local_y = local_spline(local_x);
    // Distance to (x, y)
    double local_dist = std::sqrt(local_x * local_x + local_y * local_y);

    // 1 mile/h = 0.44704 m/s
    double speed_si = ref_car.speed * 0.44704;
    // Get how many steps we can take along (0,0) -> (x,y) line to not
    // exceed target speed
    double N = local_dist / (POINTS_TIME_SPACING * speed_si);
    // Single step size
    double dx = local_x / N;
    std::cerr << "N=" << N << " dx="<<dx<<std::endl;

    // Fill generated point list to get 50 points
    double x = 0;
    while (generated_path_xs.size() < FULL_PATH_PTS) {
        x += dx;
        double y = local_spline(x);
        // Transform interpolated points back to map coordinates
        double t_x, t_y;
        t_x = ref_car.x + x * std::cos(ref_car.yaw) - y * std::sin(ref_car.yaw);
        t_y = ref_car.y + x * std::sin(ref_car.yaw) + y * std::cos(ref_car.yaw);
        generated_path_xs.push_back(t_x);
        generated_path_ys.push_back(t_y);
    }
    log_path("GENERATED", generated_path_xs, generated_path_ys);
}

void Planner::spline_transform_to_car_frame(
                        std::vector<double> & spline_xs,
                        std::vector<double> & spline_ys)
{
    log_path("SPLINE PTS BEFORE", spline_xs, spline_ys);
    // Transform points to CAR's reference frame
    for (size_t i = 0; i < spline_ys.size(); ++i) {
        double rx = spline_xs[i] - ref_car.x;
        double ry = spline_ys[i] - ref_car.y;
        double yaw = -ref_car.yaw; // rotating to car's ref frame
        spline_xs[i] = rx * std::cos(yaw) - ry * std::sin(yaw);
        spline_ys[i] = rx * std::sin(yaw) + ry * std::cos(yaw);
    }
    log_path("SPLINE PTS AFTER", spline_xs, spline_ys);
}

void Planner::spline_add_waypoints(std::vector<double> & spline_xs,
                                   std::vector<double> & spline_ys)
{
    // Generate spline's nodes along the track
    // We know track's XY coords and coresponding Frenet coord's
    for (size_t i = 1; i < 4; ++i) {
        // Generate spare points for spline
        std::pair<double, double> next_waypoint = getXY(
                                        ref_car.s + i * WAYPOINTS_S_SPACING,
                                        2 + 4 * ref_car.lane,
                                        map_waypoints_s,
                                        map_waypoints_x,
                                        map_waypoints_y);
        spline_xs.push_back(next_waypoint.first);
        spline_ys.push_back(next_waypoint.second);
    }
}

void Planner::spline_get_start_points(
                        Car const & car_current,
                        double end_path_s,
                        double end_path_d,
                        std::vector<double> & spline_xs,
                        std::vector<double> & spline_ys,
                        std::vector<double> const & previous_path_x,
                        std::vector<double> const & previous_path_y)
{
    const size_t pts_left = previous_path_x.size();
    double x0, y0, x1, y1, yaw;
    if (pts_left > 1) {
        // At least two points left from the old trajectory.
        // Continue to get a smooth curve
        x0 = previous_path_x[pts_left - 2];
        y0 = previous_path_y[pts_left - 2];
        x1 = previous_path_x[pts_left - 1];
        y1 = previous_path_y[pts_left - 1];
        yaw = std::atan2(y1 - y0, x1 - x0);
        ref_car.yaw = yaw;
        ref_car.x = x1;
        ref_car.y = y1;
        ref_car.s = end_path_s;
        ref_car.d = end_path_d;
    } else {
        ref_car.x = car_current.x;
        ref_car.y = car_current.y;
        ref_car.s = car_current.s;
        // Start from the scratch
        // Estimate one point back to start a curve
        x0 = ref_car.x - std::cos(ref_car.yaw);
        y0 = ref_car.y - std::sin(ref_car.yaw);
        // Get current car's point
        x1 = ref_car.x;
        y1 = ref_car.y;
        yaw = ref_car.yaw;
    }

    spline_xs.push_back(x0);
    spline_ys.push_back(y0);
    spline_xs.push_back(x1);
    spline_ys.push_back(y1);
}

void Planner::log_path(std::string title,
                       std::vector<double>  const & xs,
                       std::vector<double> const & ys)
{
    if (!logging) {
        return;
    }

    std::cerr << "path: " << title << ", size = " << xs.size() << std::endl;
    std::cerr << "X:";
    std::cerr << std::fixed << std::setprecision(2);
    for (auto const & x : xs) {
        std::cerr.width(7);
        std::cerr << x << " ";
    }
    std::cerr << std::endl;
    std::cerr << "Y:";
    for (auto const & y : ys) {
        std::cerr.width(7);
        std::cerr << y << " ";
    }
    std::cerr << "\n";
}

void Planner::log_car(Car const & car)
{
    if (!logging) {
        return;
    }
    std::cerr << "\nCar: x=";
    std::cerr << std::fixed << std::setprecision(2) << car.x;
    std::cerr << " y=";
    std::cerr << std::fixed << std::setprecision(2) << car.y;
    std::cerr << " yaw=";
    std::cerr << std::fixed << std::setprecision(2) << car.yaw;
    std::cerr << " s=";
    std::cerr << std::fixed << std::setprecision(2) << car.s;
    std::cerr << " d=";
    std::cerr << std::fixed << std::setprecision(2) << car.d;
    std::cerr << " v=";
    std::cerr << std::fixed << std::setprecision(2) << car.speed;
    std::cerr << " lane=";
    std::cerr << std::fixed << std::setprecision(2) << car.lane;
    std::cerr << std::endl;

}
