#include "car.h"

#include <cmath>


Car::Car() {
    id = -1;
    x = 0;
    y = 0;
    vx = 0;
    vy = 0;
    s = 0;
    d = 0;
}

Car::Car(double x, double  y, double s, double d, double yaw, double speed)
{
    this->id = -1;
    this->x = x;
    this->y = y;
    this->vx = speed * std::cos(yaw);
    this->vy = speed * std::sin(yaw);
    this->speed = speed;
    this->yaw = yaw;
    this->s = s;
    this->d = d;
    set_lane();
}

Car::Car(std::vector<double> const & car_data) {
    /* Car data from sensor fusion:
     *  - car's unique ID
     *  - car's x position in map coordinates
     *  - car's y position in map coordinates
     *  - car's x velocity in m/s
     *  - car's y velocity in m/s
     *  - car's s position in frenet coordinates
     *  - car's d position in frenet coordinates. */
    id = car_data[0];
    x = car_data[1];
    y = car_data[2];
    vx = car_data[3];
    vy = car_data[4];
    s = car_data[5];
    d = car_data[6];
    speed = std::sqrt(vx*vx + vy*vy);
    const double eps = 0.001;
    if (vx < eps && vy < eps) {
        yaw = 0; // TODO should use historical data
    } else {
        yaw = std::atan2(vx, vy);
    }
    set_lane();
}

bool Car::same_lane(Car const & other_car) const
{
    return lane == other_car.lane;
}

bool Car::left_lane(Car const & other_car) const
{
    if (lane == LANE_RIGHT && other_car.lane == LANE_CENTER) {
        return true;
    }
    if (lane == LANE_CENTER && other_car.lane == LANE_LEFT) {
        return true;
    }
    return false;
}

bool Car::right_lane(Car const & other_car) const
{
    if (lane == LANE_LEFT && other_car.lane == LANE_CENTER) {
        return true;
    }
    if (lane == LANE_CENTER && other_car.lane == LANE_RIGHT) {
        return true;
    }
    return false;
}

void Car::set_lane() {
    if (0 <= d && d < 4) {
        lane = LANE_LEFT;
    } else if (4 <= d && d < 8) {
        lane = LANE_CENTER;
    } else if (8 <= d && d < 12) {
        lane = LANE_RIGHT;
    } else {
        lane = LANE_UNKNOWN;
    }
}
