#ifndef PLANNER_H_
#define PLANNER_H_

#include "car.h"

#include "spline.h"

#include <vector>
#include <string>

class Planner {
public:
    Planner(std::vector<double> const & map_waypoints_x,
            std::vector<double> const & map_waypoints_y,
            std::vector<double> const & map_waypoints_s);

    void generate_path(Car const &car,
                       std::vector<Car> const & other_cars,
                       std::vector<double> const & previous_path_x,
                       std::vector<double> const & previous_path_y,
                       double end_path_s,
                       double end_path_d,
                       std::vector<double> &xs,
                       std::vector<double> &ys);
private:

    void spline_get_start_points(
                        Car const & car,
                        double end_path_s,
                        double end_path_d,
                        std::vector<double> & spline_xs,
                        std::vector<double> & spline_ys,
                        std::vector<double> const & previous_path_x,
                        std::vector<double> const & previous_path_y);

    void check_other_cars(std::vector<Car> const & other_cars, size_t pts_left);

    void spline_transform_to_car_frame(
                        std::vector<double> & spline_xs,
                        std::vector<double> & spline_ys);

    void spline_add_waypoints(
                        std::vector<double> & spline_xs,
                        std::vector<double> & spline_ys);

    void interpolate_dense_path_from_local_spline(
                        tk::spline const & local_spline,
                        std::vector<double> & generated_path_xs,
                        std::vector<double> & generated_path_ys);

    void log_path(std::string title, std::vector<double>  const & xs,
                                 std::vector<double> const & ys);
    void log_car(Car const & car);

private:
    std::vector<double> const & map_waypoints_x;
    std::vector<double> const & map_waypoints_y;
    std::vector<double> const & map_waypoints_s;

    bool logging;

    bool lane_blocked_left = false;
    bool lane_blocked_right = false;
    bool lane_blocked_current = false;
    bool change_lane = false;

    Car ref_car;

private:
    const double FULL_PATH_PTS = 50;
    const double MAX_SPEED = 49;
    const double MAX_ACCEL = 10;
    const double MAX_JERK = 10;
    const double POINTS_TIME_SPACING = 0.02;
    const double CAR_CLOSE_THRESHOLD = 30;
    const double CHANGE_LANE_SPACE_THRESHOLD = 20;
    const double CHANGE_LANE_SPACE_THRESHOLD_BACK = 5;
    const double WAYPOINTS_S_SPACING = 30;
};

#endif
